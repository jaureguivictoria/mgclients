<?php

namespace MGClients\BackendBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use MGClients\EntitiesBundle\Entity\Payment;
use MGClients\EntitiesBundle\Form\PaymentType;

/**
 * Payment controller.
 *
 * @Route("/payments")
 */
class PaymentController extends Controller
{
    public $entity_singular = 'Payment';
    public $header = 'Payments';
    public $icon = 'fa-money';
    /**
     * Lists all Payment entities.
     *
     * @Route("/", name="payments_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $payments = $em->getRepository('MGClientsEntitiesBundle:Payment')->findBy(array('deleted' => false));
        $delete_forms = array();

        foreach ($payments as $payment) {
            $delete_forms[] = $this->createDeleteForm($payment)->createView();
        }

        return $this->render('MGClientsBackendBundle:payment:index.html.twig', array(
            'payments' => $payments,
            'icon' => $this->icon,
            'header' => $this->header,
            'delete_forms' => $delete_forms
        ));
    }

    /**
     * Creates a new Payment entity.
     *
     * @Route("/new", name="payments_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $payment = new Payment();
        $payment->setDate(new \DateTime());
        $form = $this->createForm('MGClients\BackendBundle\Form\PaymentType', $payment);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            $em->getRepository('MGClientsEntitiesBundle:Payment')->setDueDate($payment);

            return $this->redirectToRoute('payments_index');
        }

        return $this->render('MGClientsBackendBundle:Base:new.html.twig', array(
            'payment' => $payment,
            'entity_singular' => $this->entity_singular,
            'icon' => $this->icon,
            'header' => $this->header,
            'form' => $form->createView(),
            'info' => 'Creating a new Payment automatically updates the ad\'s due date'
        ));
    }

    /**
     * Displays a form to edit an existing Payment entity.
     *
     * @Route("/{id}/edit", name="payments_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Payment $payment)
    {
        $deleteForm = $this->createDeleteForm($payment);
        $editForm = $this->createForm('MGClients\BackendBundle\Form\PaymentType', $payment);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();

            $em->getRepository('MGClientsEntitiesBundle:Payment')->setDueDate($payment);

            return $this->redirectToRoute('payments_index');
        }

        return $this->render('MGClientsBackendBundle:Base:edit.html.twig', array(
            'payment' => $payment,
            'edit_form' => $editForm->createView(),
            'entity_singular' => $this->entity_singular,
            'icon' => $this->icon,
            'header' => $this->header,
            'delete_form' => $deleteForm->createView(),
            'info' => 'Creating a new Payment automatically updates the ad\'s due date'
        ));
    }

    /**
     * Deletes a Payment entity.
     *
     * @Route("/{id}", name="payments_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Payment $payment)
    {
        $form = $this->createDeleteForm($payment);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $payment->delete();
            $em->persist($payment);
            $em->flush();
        }

        return $this->redirectToRoute('payments_index');
    }

    /**
     * Creates a form to delete a Payment entity.
     *
     * @param Payment $payment The Payment entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Payment $payment)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('payments_delete', array('id' => $payment->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
