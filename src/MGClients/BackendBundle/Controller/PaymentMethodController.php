<?php

namespace MGClients\BackendBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use MGClients\EntitiesBundle\Entity\PaymentMethod;
use MGClients\EntitiesBundle\Form\PaymentMethodType;

/**
 * PaymentMethod controller.
 *
 * @Route("/payment_methods")
 */
class PaymentMethodController extends Controller
{
    public $entity_singular = 'Payment Method';
    public $header = 'Payment Methods';
    public $icon = 'fa-money';
    /**
     * Lists all PaymentMethod entities.
     *
     * @Route("/", name="payment_methods_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $payment_methods = $em->getRepository('MGClientsEntitiesBundle:PaymentMethod')->findBy(array('deleted' => false));
        $delete_forms = array();

        foreach ($payment_methods as $type) {
            $delete_forms[] = $this->createDeleteForm($type)->createView();
        }

        return $this->render('MGClientsBackendBundle:paymentmethod:index.html.twig', array(
            'payment_methods' => $payment_methods,
            'icon' => $this->icon,
            'header' => $this->header,
            'delete_forms' => $delete_forms
        ));
    }

    /**
     * Creates a new PaymentMethod entity.
     *
     * @Route("/new", name="payment_methods_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $paymentType = new PaymentMethod();
        $form = $this->createForm('MGClients\BackendBundle\Form\PaymentMethodType', $paymentType);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($paymentType);
            $em->flush();

            return $this->redirectToRoute('payment_methods_index');
        }

        return $this->render('MGClientsBackendBundle:Base:new.html.twig', array(
            'paymentType' => $paymentType,
            'entity_singular' => $this->entity_singular,
            'icon' => $this->icon,
            'header' => $this->header,
            'form' => $form->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing PaymentMethod entity.
     *
     * @Route("/{id}/edit", name="payment_methods_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, PaymentMethod $paymentType)
    {
        $deleteForm = $this->createDeleteForm($paymentType);
        $editForm = $this->createForm('MGClients\BackendBundle\Form\PaymentMethodType', $paymentType);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($paymentType);
            $em->flush();

            return $this->redirectToRoute('payment_methods_index');
        }

        return $this->render('MGClientsBackendBundle:Base:edit.html.twig', array(
            'paymentType' => $paymentType,
            'edit_form' => $editForm->createView(),
            'entity_singular' => $this->entity_singular,
            'icon' => $this->icon,
            'header' => $this->header,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a PaymentMethod entity.
     *
     * @Route("/{id}", name="payment_methods_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, PaymentMethod $paymentType)
    {
        $form = $this->createDeleteForm($paymentType);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $paymentType->delete();
            $em->persist($paymentType);
            $em->flush();
        }

        return $this->redirectToRoute('payment_methods_index');
    }

    /**
     * Creates a form to delete a PaymentMethod entity.
     *
     * @param PaymentMethod $paymentType The PaymentMethod entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(PaymentMethod $paymentType)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('payment_methods_delete', array('id' => $paymentType->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
