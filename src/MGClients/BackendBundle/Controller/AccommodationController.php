<?php

namespace MGClients\BackendBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use MGClients\EntitiesBundle\Entity\Accommodation;
use MGClients\EntitiesBundle\Form\AccommodationType;

/**
 * Accommodation controller.
 *
 * @Route("/accommodations")
 */
class AccommodationController extends Controller
{
    public $entity_singular = 'Accommodation';
    public $header = 'Accommodations';
    public $icon = 'fa-h-square';

    /**
     * Lists all Accommodation entities.
     *
     * @Route("/", name="accommodations_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $accommodations = $em->getRepository('MGClientsEntitiesBundle:Accommodation')->findBy(array('deleted' => false));
        $delete_forms = array();

        foreach ($accommodations as $accommodation) {
            $delete_forms[] = $this->createDeleteForm($accommodation)->createView();
        }

        return $this->render('MGClientsBackendBundle:accommodation:index.html.twig', array(
            'accommodations' => $accommodations,
            'icon' => $this->icon,
            'header' => $this->header,
            'delete_forms' => $delete_forms
        ));
    }

    /**
     * Creates a new Accommodation entity.
     *
     * @Route("/new", name="accommodations_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $accommodation = new Accommodation();
        $accommodation->setPhones(array("","",""));
        $form = $this->createForm('MGClients\BackendBundle\Form\AccommodationType', $accommodation);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($accommodation);
            $em->flush();

            return $this->redirectToRoute('accommodations_index');
        }

        return $this->render('MGClientsBackendBundle:Base:new.html.twig', array(
            'accommodation' => $accommodation,
            'entity_singular' => $this->entity_singular,
            'icon' => $this->icon,
            'header' => $this->header,
            'form' => $form->createView(),
        ));
    }


    /**
     * Displays a form to edit an existing Accommodation entity.
     *
     * @Route("/{id}/edit", name="accommodations_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Accommodation $accommodation)
    {
        $deleteForm = $this->createDeleteForm($accommodation);
        $editForm = $this->createForm('MGClients\BackendBundle\Form\AccommodationType', $accommodation);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($accommodation);
            $em->flush();

            return $this->redirectToRoute('accommodations_index');
        }

        return $this->render('MGClientsBackendBundle:Base:edit.html.twig', array(
            'accommodation' => $accommodation,
            'entity_singular' => $this->entity_singular,
            'icon' => $this->icon,
            'header' => $this->header,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a Accommodation entity.
     *
     * @Route("/{id}", name="accommodations_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Accommodation $accommodation)
    {
        $form = $this->createDeleteForm($accommodation);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $accommodation->delete();
            $em->persist($accommodation);
            $em->flush();
        }

        return $this->redirectToRoute('accommodations_index');
    }

    /**
     * Creates a form to delete a Accommodation entity.
     *
     * @param Accommodation $accommodation The Accommodation entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Accommodation $accommodation)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('accommodations_delete', array('id' => $accommodation->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
