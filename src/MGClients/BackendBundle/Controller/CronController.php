<?php

namespace MGClients\BackendBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Monolog\Logger;
use Monolog\Handler\StreamHandler;
use MGClients\EntitiesBundle\Entity\Ad;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Cron controller.
 *
 * @Route("/cron")
 */
class CronController extends Controller
{
    /**
     * Automatically deactivates the expired unpayed ads. Must be run daily.
     *
     * @Route("/deactivations", name="cron_deactivations")
     * @Method("GET")
     */
    public function deactivateExpiredAdsAction()
    {
        $em = $this->getDoctrine()->getManager();

        $repository = $em->getRepository('MGClientsEntitiesBundle:Ad');
        $payment_repository = $em->getRepository('MGClientsEntitiesBundle:Payment');

        $expired_ads = $repository->findExpiredYesterday();

        foreach ($expired_ads as $ad) {

            if($payment_repository->isUnpayed($ad)){
                $ad->setActive(false);
                $em->persist($ad);
                $em->flush();
            } else {
                // TODO: Is payed but expires now
            }

            $this->noticeClientViaEmail('deactivated_ad', $ad);
        }

        return new JsonResponse(array('deactivated' => sizeof($expired_ads)));
    }

    /**
     * Finds the ads that expire in exactly 7 and 14 days from now and sends a reminder
     * Must be run daily.
     *
     * @Route("/send_expiration_reminders", name="send_expiration_reminders")
     * @Method("GET")
     */
    public function sendExpirationRemindersAction()
    {
        $em = $this->getDoctrine()->getManager();

        $repository = $em->getRepository('MGClientsEntitiesBundle:Ad');

        $adsThatExpireInAWeek = $repository->findNextExpirations('+7 days');

        foreach ($adsThatExpireInAWeek as $ad) {

            $this->noticeClientViaEmail('expiration_reminder', $ad, 7);
        }

        $adsThatExpireIn2Weeks = $repository->findNextExpirations('+14 days');

        foreach ($adsThatExpireIn2Weeks as $ad) {

            $this->noticeClientViaEmail('expiration_reminder', $ad, 14);
        }

        return new JsonResponse(array(
            '7d_notice' => sizeof($adsThatExpireInAWeek),
            '14d_notice' => sizeof($adsThatExpireIn2Weeks)
        ));
    }

    /**
    * Depending on the reason this function chooses which email to send to the client
    *
    * @param $reason string
    * @param $ad MGClientsEntitiesBundle:Ad
    * @param $x_days integer How many days are until expiration (optional)
    */
    private function noticeClientViaEmail($reason, $ad, $x_days = null)
    {
        $logger = $this->get('logger');
        $subject = "MGClients notice";

        $accommodation = $ad->getAccommodation();
        $client = $accommodation->getClient();

        switch ($reason) {
            case 'deactivated_ad':
                $template = 'MGClientsBackendBundle:Emails:deactivation.html.twig';
                $subject = "Your ad has been deactivated";
                break;
            case 'expiration_reminder':
                $template = 'MGClientsBackendBundle:Emails:expiration_reminder.html.twig';
                $subject = "Your ad expires in ".$x_days." days";
                break;
        }

        $message = \Swift_Message::newInstance()
        ->setSubject($subject)
        ->setFrom('info@mgclients.net')
        ->setTo($client->getEmail())
        ->setBody(
            $this->renderView(
                $template,
                array(
                    'ad' => $ad,
                    'subject' => $subject,
                    'x_days' => $x_days
                )
            ),
            'text/html'
        );

        try {
            $this->get('mailer')->send($message);
            $logger->info('Sent deactivation email to '.$client->getEmail().' for ad ID '.$ad->getId());
        } catch (Exception $e) {
            $logger->error('Failed to send deactivation email to '.$client->getEmail().' for ad ID '.$ad->getId());
        }

    }

    /**
    * This is a test method for visualizing an email
    * @Route("/viewmail", name="viewmail")
    * @Method("GET")
    */
    public function viewMailAction(){
        $em = $this->getDoctrine()->getManager();
        $ad = $em->getRepository('MGClientsEntitiesBundle:Ad')->find(1);
        $x_days = 7;
        return $this->render('MGClientsBackendBundle:Emails:expiration_reminder.html.twig', array(
            'subject' => "Your ad expires in ".$x_days." days",
            'ad' => $ad,
            'x_days' => $x_days
        ));
    }

    /**
    * Test method for sending an email to my personal email address
    * @Route("/mailtest", name="mailtest")
    * @Method("GET")
    */
    public function testMailAction(){
        $result;
        $logger = $this->get('logger');

        $em = $this->getDoctrine()->getManager();
        $ad = $em->getRepository('MGClientsEntitiesBundle:Ad')->find(1);

        $subject = "MGClients notice";
        $to ='jaureguivictoria@gmail.com';
        $message = \Swift_Message::newInstance()
        ->setSubject($subject)
        ->setFrom('info@mgclients.net')
        ->setTo($to)
        ->setBody(
            $this->renderView('MGClientsBackendBundle:Emails:deactivation2.html.twig',
                array('subject' => $subject, 'ad' => $ad)
            ),
            'text/html'
        );

        $result = $this->get('mailer')->send($message);

        return new JsonResponse(array('status' => $result));
    }

}
