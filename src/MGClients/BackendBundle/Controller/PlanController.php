<?php

namespace MGClients\BackendBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use MGClients\EntitiesBundle\Entity\Plan;
use MGClients\EntitiesBundle\Form\PlanType;

/**
 * Plan controller.
 *
 * @Route("/plans")
 */
class PlanController extends Controller
{
    public $entity_singular = 'Plan';
    public $header = 'Plans';
    public $icon = 'fa-file-text-o';

    /**
     * Lists all Plan entities.
     *
     * @Route("/", name="plans_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $plans = $em->getRepository('MGClientsEntitiesBundle:Plan')->findBy(array('deleted' => false));
        $delete_forms = array();

        foreach ($plans as $plan) {
            $delete_forms[] = $this->createDeleteForm($plan)->createView();
        }

        return $this->render('MGClientsBackendBundle:plan:index.html.twig', array(
            'plans' => $plans,
            'icon' => $this->icon,
            'header' => $this->header,
            'delete_forms' => $delete_forms
        ));
    }

    /**
     * Creates a new Plan entity.
     *
     * @Route("/new", name="plans_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $plan = new Plan();
        $form = $this->createForm('MGClients\BackendBundle\Form\PlanType', $plan);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($plan);
            $em->flush();

            return $this->redirectToRoute('plans_index');
        }

        return $this->render('MGClientsBackendBundle:Base:new.html.twig', array(
            'plan' => $plan,
            'entity_singular' => $this->entity_singular,
            'icon' => $this->icon,
            'header' => $this->header,
            'form' => $form->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing Plan entity.
     *
     * @Route("/{id}/edit", name="plans_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Plan $plan)
    {
        $deleteForm = $this->createDeleteForm($plan);
        $editForm = $this->createForm('MGClients\BackendBundle\Form\PlanType', $plan);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($plan);
            $em->flush();

            return $this->redirectToRoute('plans_index');
        }

        return $this->render('MGClientsBackendBundle:Base:edit.html.twig', array(
            'plan' => $plan,
            'entity_singular' => $this->entity_singular,
            'icon' => $this->icon,
            'header' => $this->header,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a Plan entity.
     *
     * @Route("/{id}", name="plans_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Plan $plan)
    {
        $form = $this->createDeleteForm($plan);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $plan->delete();
            $em->persist($plan);
            $em->flush();
        }

        return $this->redirectToRoute('plans_index');
    }

    /**
     * Creates a form to delete a Plan entity.
     *
     * @param Plan $plan The Plan entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Plan $plan)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('plans_delete', array('id' => $plan->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
