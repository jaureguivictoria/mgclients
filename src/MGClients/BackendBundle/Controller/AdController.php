<?php

namespace MGClients\BackendBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use MGClients\EntitiesBundle\Entity\Ad;
use MGClients\EntitiesBundle\Form\AdType;

/**
 * Ad controller.
 *
 * @Route("/ads")
 */
class AdController extends Controller
{
    public $entity_singular = 'Ad';
    public $header = 'Ads';
    public $icon = 'fa-star-o';

    /**
     * Lists all Ad entities.
     *
     * @Route("/", name="ads_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $ads = $em->getRepository('MGClientsEntitiesBundle:Ad')->findBy(array('deleted' => false));
        $plans = $delete_forms = array();

        foreach ($ads as $ad) {
            $plan = $em->getRepository('MGClientsEntitiesBundle:Plan')->findPlan($ad);
            $plans[] = !empty($plan) ? $plan->getName() : 'No payments done yet';
            $delete_forms[] = $this->createDeleteForm($ad)->createView();
        }

        return $this->render('MGClientsBackendBundle:ad:index.html.twig', array(
            'ads' => $ads,
            'icon' => $this->icon,
            'header' => $this->header,
            'delete_forms' => $delete_forms,
            'plans' => $plans
        ));
    }

    /**
     * Creates a new Ad entity.
     *
     * @Route("/new", name="ads_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $ad = new Ad();
        $ad->setActive(true);
        $form = $this->createForm('MGClients\BackendBundle\Form\AdType', $ad);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($ad);
            $em->flush();

            return $this->redirectToRoute('ads_index');
        }

        return $this->render('MGClientsBackendBundle:Base:new.html.twig', array(
            'ad' => $ad,
            'icon' => $this->icon,
            'header' => $this->header,
            'entity_singular' => $this->entity_singular,
            'form' => $form->createView(),
            'info' => 'Register a payment for this ad afterwards'
        ));
    }

    /**
     * Displays a form to edit an existing Ad entity.
     *
     * @Route("/{id}/edit", name="ads_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Ad $ad)
    {
        $deleteForm = $this->createDeleteForm($ad);
        $editForm = $this->createForm('MGClients\BackendBundle\Form\AdEditType', $ad);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($ad);
            $em->flush();

            return $this->redirectToRoute('ads_index');
        }

        return $this->render('MGClientsBackendBundle:Base:edit.html.twig', array(
            'ad' => $ad,
            'entity_singular' => $this->entity_singular,
            'icon' => $this->icon,
            'header' => $this->header,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a Ad entity.
     *
     * @Route("/{id}", name="ads_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Ad $ad)
    {
        $form = $this->createDeleteForm($ad);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $ad->delete();
            $em->persist($ad);
            $em->flush();
        }

        return $this->redirectToRoute('ads_index');
    }

    /**
     * Creates a form to delete a Ad entity.
     *
     * @param Ad $ad The Ad entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Ad $ad)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('ads_delete', array('id' => $ad->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
