<?php

namespace MGClients\BackendBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use MGClients\EntitiesBundle\Entity\AccommodationType;
use MGClients\BackendBundle\Form\AccommodationTypeType;

/**
 * AccommodationType controller.
 *
 * @Route("/accommodation_types")
 */
class AccommodationTypeController extends Controller
{

    public $entity_singular = "Accommodation Type";
    public $header = 'Accommodation Types';
    public $icon = 'fa-h-square';

    /**
     * Lists all AccommodationType entities.
     *
     * @Route("/", name="accommodation_types_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $accommodationTypes = $em->getRepository('MGClientsEntitiesBundle:AccommodationType')->findBy(array('deleted' => false));
        $delete_forms = array();

        foreach ($accommodationTypes as $type) {
            $delete_forms[] = $this->createDeleteForm($type)->createView();
        }

        return $this->render('MGClientsBackendBundle:accommodationtype:index.html.twig', array(
            'accommodationTypes' => $accommodationTypes,
            'icon' => $this->icon,
            'header' => $this->header,
            'delete_forms' => $delete_forms
        ));
    }

    /**
     * Creates a new AccommodationType entity.
     *
     * @Route("/new", name="accommodation_types_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $accommodationType = new AccommodationType();
        $form = $this->createForm('MGClients\BackendBundle\Form\AccommodationTypeType', $accommodationType);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($accommodationType);
            $em->flush();

            return $this->redirectToRoute('accommodation_types_index');
        }

        return $this->render('MGClientsBackendBundle:Base:new.html.twig', array(
            'accommodationType' => $accommodationType,
            'icon' => $this->icon,
            'header' => $this->header,
            'entity_singular' => $this->entity_singular,
            'form' => $form->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing AccommodationType entity.
     *
     * @Route("/{id}/edit", name="accommodation_types_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, AccommodationType $accommodationType)
    {
        $deleteForm = $this->createDeleteForm($accommodationType);
        $editForm = $this->createForm('MGClients\BackendBundle\Form\AccommodationTypeType', $accommodationType);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($accommodationType);
            $em->flush();

            return $this->redirectToRoute('accommodation_types_index');
        }

        return $this->render('MGClientsBackendBundle:Base:edit.html.twig', array(
            'accommodationType' => $accommodationType,
            'entity_singular' => $this->entity_singular,
            'icon' => $this->icon,
            'header' => $this->header,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a AccommodationType entity.
     *
     * @Route("/{id}", name="accommodation_types_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, AccommodationType $accommodationType)
    {
        $form = $this->createDeleteForm($accommodationType);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $accommodationType->delete();
            $em->persist($accommodationType);
            $em->flush();
        }

        return $this->redirectToRoute('accommodation_types_index');
    }

    /**
     * Creates a form to delete a AccommodationType entity.
     *
     * @param AccommodationType $accommodationType The AccommodationType entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(AccommodationType $accommodationType)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('accommodation_types_delete', array('id' => $accommodationType->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
