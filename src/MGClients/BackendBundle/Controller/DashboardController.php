<?php

namespace MGClients\BackendBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Monolog\Logger;

class DashboardController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
        $repository = $em->getRepository('MGClientsEntitiesBundle:Ad');

        $upcomingExpirations = $repository->findUpcomingExpirations();
        $expired = $repository->findExpired();
        $actives = $repository->findActiveAds();

        return $this->render('MGClientsBackendBundle:Dashboard:index.html.twig', array(
            'icon' => 'fa-home',
            'header' => 'Dashboard',
            'expired' => $expired,
            'actives' => $actives,
            'upcomingExpirations' => $upcomingExpirations
        ));
    }
}
