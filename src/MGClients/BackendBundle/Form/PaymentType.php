<?php

namespace MGClients\BackendBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

class PaymentType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('date', DatetimeType::class)
            ->add('amount', MoneyType::class, array('currency' => 'ARS'))
            ->add('responsible')
            ->add('plan', EntityType::class, array(
                'class' => 'MGClientsEntitiesBundle:Plan',
                'choice_label' => 'name '
            ))
            ->add('method')
            ->add('ad')
            ->add('save', SubmitType::class, array('attr' => array('class' =>'btn btn-primary')))
        ;
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'MGClients\EntitiesBundle\Entity\Payment'
        ));
    }
}
