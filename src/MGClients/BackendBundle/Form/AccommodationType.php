<?php

namespace MGClients\BackendBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\UrlType;

class AccommodationType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name')
            ->add('description')
            ->add('address')
            ->add('website', UrlType::class, array('required' => false))
            ->add('email', EmailType::class, array('required' => false))
            ->add('phones', CollectionType::class, array(
                // each entry in the array will be a "string" field
                'entry_type'   => TextType::class,
                 'entry_options' => array(
                     //'attr' => array('class' => 'voption')
                     'required' => false
                ),
                'allow_add' => true,
                'allow_delete' => true,
                'attr' => array('class' => 'form-group')

            ))
            ->add('coordinates')
            ->add('type')
            ->add('client')
            ->add('save', SubmitType::class, array('attr' => array('class' =>'btn btn-primary')))
        ;
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'MGClients\EntitiesBundle\Entity\Accommodation'
        ));
    }
}
