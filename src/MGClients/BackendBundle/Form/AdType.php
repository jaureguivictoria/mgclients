<?php

namespace MGClients\BackendBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Doctrine\Bundle\DoctrineBundle\Registry as Doctrine;

class AdType extends AbstractType
{
    /** @var \Doctrine\ORM\EntityManager */
    private $em;

    /**
     * Constructor
     *
     * @param Doctrine $doctrine
     */
    public function __construct(Doctrine $doctrine)
    {
        $this->em = $doctrine->getManager();
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $accommodation_repo = $this->em->getRepository('MGClientsEntitiesBundle:Accommodation');
        $accommodations = $accommodation_repo->getAccommodationsWithoutAd();

        $builder
            ->add('active')
            ->add('dueDate', DatetimeType::class)
            ->add('accommodation', EntityType::class, array(
                'class' => 'MGClients\EntitiesBundle\Entity\Accommodation',
                'choices' => $accommodations
            ))
            ->add('save', SubmitType::class, array('attr' => array('class' =>'btn btn-primary')))
        ;
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'MGClients\EntitiesBundle\Entity\Ad'
        ));
    }
}
