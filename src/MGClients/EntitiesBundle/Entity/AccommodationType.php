<?php

namespace MGClients\EntitiesBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * AccommodationType
 *
 * @ORM\Table(name="accommodation_type")
 * @ORM\Entity(repositoryClass="MGClients\EntitiesBundle\Repository\AccommodationTypeRepository")
 */
class AccommodationType extends Base
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=255, unique=true)
     */
    private $type;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set type
     *
     * @param string $type
     *
     * @return AccommodationType
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    public function __toString() {
        return $this->type;
    }
}
