<?php

namespace MGClients\EntitiesBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Client
 *
 * @ORM\Table(name="client")
 * @ORM\Entity(repositoryClass="MGClients\EntitiesBundle\Repository\ClientRepository")
 */
class Client extends Base
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=255, unique=true, nullable=false)
     */
    private $email;

    /**
     * @var string
     *
     * @ORM\Column(name="surname", type="string", length=255, nullable=true)
     */
    private $surname;

    /**
     * @var string
     *
     * @ORM\Column(name="address", type="string", length=255)
     */
    private $address;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="\MGClients\EntitiesBundle\Entity\User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="user_id", referencedColumnName="id", nullable=false)
     * })
     */
    protected $user;

    /**
     * @ORM\OneToMany(targetEntity="Accommodation", mappedBy="client")
     */
    private $accommodations;

    public function __construct()
    {
        $this->accommodations = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Client
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return Client
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set address
     *
     * @param string $address
     *
     * @return Client
     */
    public function setAddress($address)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * Get address
     *
     * @return string
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Set surname
     *
     * @param string $surname
     *
     * @return Client
     */
    public function setSurname($surname)
    {
        $this->surname = $surname;

        return $this;
    }

    /**
     * Get surname
     *
     * @return string
     */
    public function getSurname()
    {
        return $this->surname;
    }

    /**
     * Set user
     *
     * @param \MGClients\EntitiesBundle\Entity\User $user
     * @return Customer
     */
    public function setUser(\MGClients\EntitiesBundle\Entity\User $user = null) {
        $this->user = $user;

        return $this;
    }
    /**
     * Get user
     *
     * @return \MGClients\EntitiesBundle\Entity\User
     */
    public function getUser() {
        return $this->user;
    }

    /**
     * Add accommodation
     *
     * @param \MGClients\EntitiesBundle\Entity\Accommodation $accommodation
     * @return Accommodation
     */
    public function addAccommodation(\MGClients\EntitiesBundle\Entity\Accommodation $accommodation = null) {
        $this->accommodations[] = $accommodation;

        return $this;
    }
    /**
     * Get accommodations
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAccommodations() {
        return $this->accommodations;
    }

    /**
     * Remove accommodation
     *
     * @param \MGClients\EntitiesBundle\Entity\Accommodation $accommodation
     */
    public function removeAccommodation(\MGClients\EntitiesBundle\Entity\Accommodation $accommodation) {
        $this->accommodations->removeElement($accommodation);
    }

    public function __toString() {
        return $this->name .' '. $this->surname;
    }
}
