<?php

namespace MGClients\EntitiesBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * MGClients\EntitiesBundle\Entity\Base
 *
 * @ORM\MappedSuperclass
 * @ORM\HasLifecycleCallbacks
 */
 abstract class Base {

    /**
     * @var datetime $createdAt
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=true)
     */
    private $createdAt;

    /**
     * @var datetime $lastModification
     *
     * @ORM\Column(name="last_modification", type="datetime", nullable=true)
     */
    private $lastModification;

    /**
     * @var boolean
     *
     * @ORM\Column(name="deleted", type="boolean")
     */
    private $deleted = false;

    /**
     * @ORM\PrePersist
     */
    public function setCreatedValue() {
        $this->createdAt = new \DateTime();
        $this->lastModification = new \DateTime();
    }

    /**
     * @ORM\PreUpdate
     */
    public function setModificationValue() {
        $this->lastModification = new \DateTime();
    }

    /**
     * Set lastModification
     *
     * @param \DateTime $lastModification
     */
    public function setLastModification($lastModification){
        $this->lastModification = $lastModification;
    }

    /**
     * Get lastModification
     *
     * @return \DateTime
     */
    public function getLastModification(){
        return $this->lastModification;
    }

    /**
      * delete
      */
    public function delete(){
        $this->deleted = true;
    }

    /**
    * Is Deleted
    *
    * @return boolean
    */
    public function isDeleted(){
      return $this->deleted;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt(){
        return $this->createdAt;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     */
    public function setCreatedAt($createdAt){
        $this->createdAt = $createdAt;
    }
}
