<?php

namespace MGClients\EntitiesBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Accommodation
 *
 * @ORM\Table(name="accommodation")
 * @ORM\Entity(repositoryClass="MGClients\EntitiesBundle\Repository\AccommodationRepository")
 */
class Accommodation extends Base
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=255, nullable=true)
     */
    private $email;

    /**
     * @var string
     *
     * @ORM\Column(name="address", type="string", length=255, nullable=true)
     */
    private $address;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\Column(name="coordinates", type="string", length=255, nullable=true)
     */
    private $coordinates;

    /**
     * @var string
     *
     * @ORM\Column(name="phones", type="simple_array", nullable=true)
     */
    private $phones;

    /**
     * @var string
     *
     * @ORM\Column(name="website", type="string", length=255, nullable=true)
     */
    private $website;

    /**
     * @ORM\ManyToOne(targetEntity="AccommodationType")
     * @ORM\JoinColumn(name="type_id", referencedColumnName="id", nullable=false)
     */
    private $type;

    /**
     * @ORM\OneToMany(targetEntity="Ad", mappedBy="accommodation")
     */
    private $ads;

    /**
     * @ORM\ManyToOne(targetEntity="Client", inversedBy="accommodations")
     * @ORM\JoinColumn(name="client_id", referencedColumnName="id", nullable=false)
     */
    private $client;

    public function __construct()
    {
        $this->ads = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Accommodation
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set type
     *
     * @param \MGClients\EntitiesBundle\Entity\AccommodationType $type
     * @return Accommodation
     */
    public function setType(\MGClients\EntitiesBundle\Entity\AccommodationType $type = null) {
        $this->type = $type;

        return $this;
    }
    /**
     * Get type
     *
     * @return \MGClients\EntitiesBundle\Entity\AccommodationType
     */
    public function getType() {
        return $this->type;
    }

    /**
     * Add ad
     *
     * @param \MGClients\EntitiesBundle\Entity\Ad $ad
     * @return Accommodation
     */
    public function addAd(\MGClients\EntitiesBundle\Entity\Ad $ad = null) {
        $this->ads[] = $ad;

        return $this;
    }
    /**
     * Get ads
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAds() {
        return $this->ads;
    }

    /**
     * Remove ad
     *
     * @param \MGClients\EntitiesBundle\Entity\Ad $ad
     */
    public function removeAd(\MGClients\EntitiesBundle\Entity\Ad $ad) {
        $this->ads->removeElement($ad);
    }

    /**
     * Set client
     *
     * @param \MGClients\EntitiesBundle\Entity\Client $client
     * @return Accommodation
     */
    public function setClient(\MGClients\EntitiesBundle\Entity\Client $client = null) {
        $this->client = $client;

        return $this;
    }
    /**
     * Get client
     *
     * @return \MGClients\EntitiesBundle\Entity\Client
     */
    public function getClient() {
        return $this->client;
    }

    public function __toString() {
        return $this->name;
    }

    /**
     * Get the value of Email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set the value of Email
     *
     * @param string email
     *
     * @return Accommodation
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get the value of Address
     *
     * @return string
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Set the value of Address
     *
     * @param string address
     *
     * @return Accommodation
     */
    public function setAddress($address)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * Get the value of Description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set the value of Description
     *
     * @param string description
     *
     * @return Accommodation
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get the value of Coordinates
     *
     * @return string
     */
    public function getCoordinates()
    {
        return $this->coordinates;
    }

    /**
     * Set the value of Coordinates
     *
     * @param string coordinates
     *
     * @return Accommodation
     */
    public function setCoordinates($coordinates)
    {
        $this->coordinates = $coordinates;

        return $this;
    }

    /**
     * Get the value of Phones
     *
     * @return string
     */
    public function getPhones()
    {
        return $this->phones;
    }

    /**
     * Set the value of Phones
     *
     * @param string phones
     *
     * @return Accommodation
     */
    public function setPhones($phones)
    {
        $this->phones = $phones;

        return $this;
    }

    /**
     * Get the value of Website
     *
     * @return string
     */
    public function getWebsite()
    {
        return $this->website;
    }

    /**
     * Set the value of Website
     *
     * @param string website
     *
     * @return Accommodation
     */
    public function setWebsite($website)
    {
        $this->website = $website;

        return $this;
    }

    /**
     * Set the value of Ads
     *
     * @param mixed ads
     *
     * @return Accommodation
     */
    public function setAds($ads)
    {
        $this->ads = $ads;

        return $this;
    }

}
