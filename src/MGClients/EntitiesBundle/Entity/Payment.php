<?php

namespace MGClients\EntitiesBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Payment
 *
 * @ORM\Table(name="payment")
 * @ORM\Entity(repositoryClass="MGClients\EntitiesBundle\Repository\PaymentRepository")
 */
class Payment extends Base
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="datetime")
     */
    private $date;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="due_date", type="datetime")
     */
    private $dueDate;

    /**
     * @var float
     *
     * @ORM\Column(name="amount", type="float")
     */
    private $amount;

    /**
     * @var string
     *
     * @ORM\Column(name="responsible", type="string", length=255)
     */
    private $responsible;


    /**
     * @ORM\ManyToOne(targetEntity="Plan")
     * @ORM\JoinColumn(name="plan_id", referencedColumnName="id", nullable=false)
     */
    private $plan;

    /**
     * @ORM\ManyToOne(targetEntity="PaymentMethod")
     * @ORM\JoinColumn(name="method_id", referencedColumnName="id", nullable=false)
     */
    private $method;

    /**
     * @ORM\ManyToOne(targetEntity="Ad", inversedBy="payments", cascade={"persist"})
     * @ORM\JoinColumn(name="ad_id", referencedColumnName="id", nullable=false)
     */
    private $ad;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     *
     * @return Payment
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set dueDate
     *
     * @param \DateTime $dueDate
     *
     * @return Ad
     */
    public function setDueDate($dueDate)
    {
        $this->dueDate = $dueDate;

        return $this;
    }

    /**
     * Get dueDate
     *
     * @return \DateTime
     */
    public function getDueDate()
    {
        return $this->dueDate;
    }

    /**
     * Set amount
     *
     * @param float $amount
     *
     * @return Payment
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;

        return $this;
    }

    /**
     * Get amount
     *
     * @return float
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * Set responsible
     *
     * @param string $responsible
     *
     * @return Payment
     */
    public function setResponsible($responsible)
    {
        $this->responsible = $responsible;

        return $this;
    }

    /**
     * Get responsible
     *
     * @return string
     */
    public function getResponsible()
    {
        return $this->responsible;
    }

    /**
     * Set plan
     *
     * @param \MGClients\EntitiesBundle\Entity\Plan $plan
     * @return Payment
     */
    public function setPlan(\MGClients\EntitiesBundle\Entity\Plan $plan = null) {
        $this->plan = $plan;

        return $this;
    }
    /**
     * Get plan
     *
     * @return \MGClients\EntitiesBundle\Entity\Plan
     */
    public function getPlan() {
        return $this->plan;
    }

    /**
     * Set ad
     *
     * @param \MGClients\EntitiesBundle\Entity\Ad $ad
     * @return Payment
     */
    public function setAd(\MGClients\EntitiesBundle\Entity\Ad $ad = null) {
        $this->ad = $ad;

        return $this;
    }
    /**
     * Get ad
     *
     * @return \MGClients\EntitiesBundle\Entity\Ad
     */
    public function getAd() {
        return $this->ad;
    }

    /**
     * Set method
     *
     * @param \MGClients\EntitiesBundle\Entity\PaymentMethod $method
     * @return Payment
     */
    public function setMethod(\MGClients\EntitiesBundle\Entity\PaymentMethod $method = null) {
        $this->method = $method;

        return $this;
    }
    /**
     * Get method
     *
     * @return \MGClients\EntitiesBundle\Entity\PaymentMethod
     */
    public function getMethod() {
        return $this->method;
    }
}
