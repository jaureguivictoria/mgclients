<?php

namespace MGClients\EntitiesBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Ad
 *
 * @ORM\Table(name="ad")
 * @ORM\Entity(repositoryClass="MGClients\EntitiesBundle\Repository\AdRepository")
 */
class Ad extends Base
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="due_date", type="datetime", nullable=true)
     */
    private $dueDate;

    /**
     * @var bool
     *
     * @ORM\Column(name="active", type="boolean")
     */
    private $active;

    /**
     * @ORM\OneToMany(targetEntity="Payment", mappedBy="ad", cascade={"persist"})
     */
    private $payments;

    /**
     * @ORM\ManyToOne(targetEntity="Accommodation", inversedBy="ads", cascade={"remove"})
     * @ORM\JoinColumn(name="accommodation_id", referencedColumnName="id", nullable=false, unique=true)
     */
    private $accommodation;

    public function __construct()
    {
        $this->payments = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set dueDate
     *
     * @param \DateTime $dueDate
     *
     * @return Ad
     */
    public function setDueDate($dueDate)
    {
        $this->dueDate = $dueDate;

        return $this;
    }

    /**
     * Get dueDate
     *
     * @return \DateTime
     */
    public function getDueDate()
    {
        return $this->dueDate;
    }

    /**
     * Set active
     *
     * @param boolean $active
     *
     * @return Ad
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Get active
     *
     * @return bool
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Set accommodation
     *
     * @param \MGClients\EntitiesBundle\Entity\Accommodation $accommodation
     * @return Customer
     */
    public function setAccommodation(\MGClients\EntitiesBundle\Entity\Accommodation $accommodation = null) {
        $this->accommodation = $accommodation;

        return $this;
    }
    /**
     * Get accommodation
     *
     * @return \MGClients\EntitiesBundle\Entity\Accommodation
     */
    public function getAccommodation() {
        return $this->accommodation;
    }

    public function __toString(){
        return $this->accommodation->getName();
    }

    /**
     * Add payment
     *
     * @param \MGClients\EntitiesBundle\Entity\Payment $payment
     * @return Ad
     */
    public function addPayment(\MGClients\EntitiesBundle\Entity\Payment $payment = null) {
        $this->payments[] = $payment;

        return $this;
    }

    /**
     * Get payments
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPayments() {
        return $this->payments;
    }

}
