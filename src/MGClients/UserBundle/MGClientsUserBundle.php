<?php

namespace MGClients\UserBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class MGClientsUserBundle extends Bundle
{
    public function getParent()
    {
        return 'FOSUserBundle';
    }
}
