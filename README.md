MGClients
=========

A PHP Symfony project created on April 25, 2016, 3:14 pm.

## Overview ##

This is a Symfony Project that manages clients and their monthly payments for their subscriptions. The subscriptions correspond to accommodation ads. Each client can have multiple accommodations. The idea is to automate the email reminders so that clients pay on time. Before sending a reminder, it validates if a payment has already been done so we don't bother the client.

## Bundles Used ##

This project uses the following bundles:

* FOSUser
* Assetic
* Doctrine ORM
* Twig Templating Engine
* SwiftMailer

## Front-end ##

For the views I am using Bootstrap and Font-awesome. For compiling sass files Assetic uses leafo/scssphp, and for squeezing the javascript files patchwork/jsqueeze.

## Deployment ##
I am using Heroku and ClearDB for hosting. You can visit the site [here](http://mgclients.herokuapp.com/).